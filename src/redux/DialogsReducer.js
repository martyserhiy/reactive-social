const ADD_MESSAGE = 'ADD-MESSAGE';

let initialStorage = {
    "users": [
        {"name": 'User1', "id": 1},
        {"name": "User2", "id": 2},
        {"name": "User3", "id": 3},
    ],
    "messages": [
        {"message": "some text", "id": 1},
        {"message": "text 2", "id": 2},
        {"message": "text 3", "id": 3},
        {"message": "text 4", "id": 4},
        {"message": "text 5", "id": 5},
    ],
};

const dialogsReducer = (storage = initialStorage, action) => {
    switch (action.type) {
        case ADD_MESSAGE :
            let newMessage = {
                "message": action.message,
                "id": storage.messages.length + 1
            };

            return { ...storage, messages: [...storage.messages, newMessage] };
        default:
            return storage
    }

};

export let addMessageValueActionCreator = (message) => {
    return {
        type: ADD_MESSAGE,
        message: message
    }
};

export default dialogsReducer;