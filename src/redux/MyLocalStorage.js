import './DialogsReducer';
import './ProfileReducer';
import dialogsReducer from "./DialogsReducer";
import profileReduce from "./ProfileReducer";

let store = {
    _storage: {
        dialogs: {
            "users": [
                {"name": 'User1', "id": 1},
                {"name": "User2", "id": 2},
                {"name": "User3", "id": 3},
            ],
            "messages": [
                {"message": "some text", "id": 1},
                {"message": "text 2", "id": 2},
                {"message": "text 3", "id": 3},
                {"message": "text 4", "id": 4},
                {"message": "text 5", "id": 5},
            ],
            "currentMessageValue": '',
        },
        profile: {
            posts: [
                {"text": "some text", "likes": 123, "id": 1},
                {"text": "text 2", "likes": 13, "id": 2},
                {"text": "some text 3", "likes": 43, "id": 3},
            ],
            currentPostValue: '',
        },
    },
    _callSubscriber() {
        console.log('subscriber not defined');
    },
    _updatePostValue(currentValue) {
        this._storage.profile.currentPostValue = currentValue;
        this._callSubscriber();
    },
    _updateMessageValue(currentValue) {
        this._storage.dialogs.currentMessageValue = currentValue;
        this._callSubscriber();
    },
    _addPost() {
        let newPost = {
            "id": 4,
            "text": this._storage.profile.currentPostValue,
            "likes": 0,
        };
        this._storage.profile.posts.push(newPost);
        this._storage.profile.currentPostValue = '';
        this._callSubscriber();

    },
    _addMessage() {
        let newMessage = {
            "message": this._storage.dialogs.currentMessageValue,
            "id": "6"
        };
        this._storage.dialogs.messages.push(newMessage);
        this._storage.dialogs.currentMessageValue = '';
        this._callSubscriber();

    },
    getStorage() {
        return this._storage;
    },
    dispatch (action) {
        this._storage.dialogs = dialogsReducer(this._storage.dialogs, action);
        this._storage.profile = profileReduce(this._storage.profile, action);
        this._callSubscriber();
    },

    subscriber(observer) {
        this._callSubscriber = observer;
    }
};

export default store;