const ADD_POST = 'ADD-POST';

let initialStorage = {
    posts: [
        {"text": "some text", "likes": 123, "id": 1},
        {"text": "text 2", "likes": 13, "id": 2},
        {"text": "some text 3", "likes": 43, "id": 3},
    ],
};

const profileReducer = (storage = initialStorage, action) => {
    switch (action.type) {
        case ADD_POST :
            let newPost = {
                "id": storage.posts.length + 1,
                "text": action.text,
                "likes": 0,
            };
            return { ...storage, posts: [...storage.posts, newPost] };
        default:
            return storage;
    }

};

export let addPostActionCreator = (text) => {
    return {
        type: ADD_POST,
        text: text
    }
};

export default profileReducer;