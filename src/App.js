import React from 'react';
import Header from './Components/Header/Header';
import {Route} from "react-router-dom";
import ProfileContainer from "./Components/Profile/ProfileContainer";
import DialogsContainer from "./Components/Dialogs/DialogsContainer";
import UsersContainer from "./Components/Users/UsersContainer";

function App() {
    return (
        <div className="container-fluid">
            <div className="container">
                <Header/>
                <Route path="/profile" render={() => (
                    <ProfileContainer />
                )}/>
                <Route path="/dialogs/" render={() => (
                    <DialogsContainer />
                )}/>
                <Route path="/users/" render={() => <UsersContainer />}/>
            </div>
        </div>
    );
}

export default App;
