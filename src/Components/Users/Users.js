import React from "react";
import styles from './users.module.scss'
import userPhoto from '../../assets/images/user.png'

export default (props) => {

    return (
        <div>
            {
                props.users.map(u =>
                    <div key={u.id}>
                        <span>
                            <div>
                                <img src={u.photos.small ? u.photos.small : userPhoto} alt={u.name} className={styles.usersPhoto}/>
                            </div>
                            <div>
                                {u.followed ?
                                    <button onClick={() => {
                                        props.unfollow(u.id)}
                                    }>Unfollow</button>
                                    :
                                    <button onClick={() => props.follow(u.id)}>Follow</button>
                                }
                            </div>
                        </span>
                        <span>
                            <span>
                                <div>{u.name}</div>
                                <div>
                                    {u.status}
                                </div>
                            </span>
                            <span>
                                <div>u.location.country</div>
                                <div>u.location.city</div>
                            </span>
                        </span>
                    </div>
                )
            }
        </div>
    )
}