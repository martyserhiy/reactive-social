import React, {useEffect} from 'react';
import Users from "./Users";
import {useDispatch, useSelector} from "react-redux";
import {followActionCreator, setUsersActionCreator, unfollowActionCreator} from "../../redux/UsersReducer";

export default () => {
    const users = useSelector(state => state.users.users)
    const dispatch = useDispatch()
    const follow = (userId) => dispatch(followActionCreator(userId))
    const unfollow = (userId) => dispatch(unfollowActionCreator(userId))
    const setUsers = (users) => dispatch(setUsersActionCreator(users))
    async function getUsers(){
        const response = await fetch('https://social-network.samuraijs.com/api/1.0/users')

        return await response.json();
    }
    useEffect(() => {
        const users = getUsers()
        users.then((data) => console.log(data.items))
    }, [])

    return (
        <Users users={users} follow={follow} unfollow={unfollow} setUsers={setUsers}/>
    )
}