import React from 'react';
import logo from "../../logo.svg";
import './header.scss';
import {Link} from 'react-router-dom';

 export default class Header extends React.Component {
     constructor(props) {
         super(props);
         this.toggleNavbar = this.toggleNavbar.bind(this);
         this.state = {
             collapsed: true,
         };
     }
     toggleNavbar = () => {
         this.setState({
             collapsed: !this.state.collapsed,
         });
     };
     render() {
         const collapsed = this.state.collapsed;
         const classOne = collapsed ? 'collapse navbar-collapse' : 'collapse navbar-collapse show';
         const classTwo = collapsed ? 'navbar-toggler navbar-toggler-right collapsed' : 'navbar-toggler navbar-toggler-right';

         return (
             <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                 <button onClick={this.toggleNavbar} className={classTwo} type="button" data-toggle="collapse" data-target="#navbarToggler2"
                         aria-controls="navbarToggler2" aria-expanded="true" aria-label="Toggle navigation">
                     <span className="navbar-toggler-icon"/>
                 </button>
                 <div className={`${classOne} justify-content-center`} id="navbarToggler2">
                     <ul className="navbar-nav">
                         <li className="nav-item">
                             <Link className="nav-link" to='/'>Home</Link>
                         </li>
                         <li className="nav-item">
                             <Link className="nav-link" to="/profile">Profile</Link>
                         </li>
                         <li className="nav-item">
                             <Link className="nav-link" to="/dialogs">Dialogs</Link>
                         </li>
                         <li className="nav-item">
                             <Link className="nav-link" to="/users">Users</Link>
                         </li>
                     </ul>
                 </div>
             </nav>
         )
     }
}