import React from 'react';
import {addPostActionCreator} from "../../redux/ProfileReducer";
import Profile from "./Profile";
import {useDispatch, useSelector} from "react-redux";

export default () => {
    const state = useSelector(state => state)
    const dispatch = useDispatch()

    let addPost = (text) => {
        dispatch(addPostActionCreator(text));
    };

    return (
        <Profile addPost={addPost} posts={state.profile.posts}/>
    )
}