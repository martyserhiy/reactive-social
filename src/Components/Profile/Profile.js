import React, {useMemo, useState} from 'react';
import Post from './Post';

export default (props) => {
    const [text, setText] = useState()
    let posts = useMemo(() => {
        return props.posts.map(post => (
            <Post text={post.text} likes={post.likes} key={post.id}/>
        ));
    },[props.posts])

    let onAddPost = () => {
        props.addPost(text);
        setText('')
    };
    let onChangePostValue = (e) => {
        let value = e.target.value;
        setText(value);
    };

    return (
        <div className="container content">
            <div>
                Ava + description
            </div>
            <div>
                <div>My posts</div>
                <div>
                    <textarea onChange={onChangePostValue} placeholder="Enter new post text"
                              value={text}/>
                </div>
                <div>
                    <button className="btn btn-success" onClick={onAddPost}>Add post</button>
                </div>
            </div>
            <div className="posts">
                {posts}
            </div>
        </div>
    )
}