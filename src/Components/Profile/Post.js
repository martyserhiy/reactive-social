import React from 'react';

export default (props) => {
    return (
        <div className="post">
            <div>{props.text}</div>
            <span>likes: {props.likes}</span>
        </div>
    )
}