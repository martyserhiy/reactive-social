import React from 'react';
import {addMessageValueActionCreator} from "../../redux/DialogsReducer";
import Dialogs from "./Dialogs";
import {useDispatch, useSelector} from "react-redux";

export default () => {
    const state = useSelector(state => state)
    const dispatch = useDispatch()

    let addMessage = (message) => {
        dispatch(addMessageValueActionCreator(message));
    };

    return (
        <Dialogs addMessage={addMessage} users={state.dialogs.users}
                 messages={state.dialogs.messages}
        />
    )
};
