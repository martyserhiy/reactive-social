import React, {useMemo, useState} from 'react';
import {NavLink} from "react-router-dom";

const DialogItem = (props) => {
    return (
        <div>
            <NavLink to={`/dialogs/${props.id}`}>{props.name}</NavLink>
        </div>
    )
};

const Message = (props) => {
    return (
        <div>
            {props.message}
        </div>
    )
};

const Dialogs = (props) => {
    const [message, setMessage] = useState()
    let dialogItems = useMemo(() => {
        return props.users.map(dialog => (
            <DialogItem name={dialog.name} id={dialog.id} key={dialog.id}/>
        ));
    }, [props.users])
    let messages = useMemo(() => {
      return props.messages.map(message => (
          <Message message={message.message} key={message.id}/>
      ));
    }, [props.messages])
    let onChangeMessage = (e) => {
        let value = e.target.value;
        setMessage(value);
    };
    let onAddMessage = () => {
        props.addMessage(message);
        setMessage('')
    };

    return (
        <div className="container">
            <div className="row">
                <div className="col-sm-4 side-bar">
                    {dialogItems}
                </div>
                <div className="col-sm-8">
                    {messages}
                    <div>
                        <textarea onChange={onChangeMessage} placeholder="Enter new message"
                                  value={message}/>
                    </div>
                    <div>
                        <button onClick={onAddMessage} className="btn btn-success">Send Message</button>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default Dialogs